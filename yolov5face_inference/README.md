

## 1. Inference pytorch engine model  
``` 
    python3 testing_tensorrt.py
```

## Note:
Config use in code:  
USE_CUDA: True /False

python: 3.7.11  
pytorch: 1.8.0  
cuda: 11.1   
cuDNN: 8.0.4  
tensorrt: 8.0.3.4  
onnx: 1.11.0  
onnxruntime: 1.11.0 
