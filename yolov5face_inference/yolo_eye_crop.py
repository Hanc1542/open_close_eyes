
from ast import BitXor
import math
import os
import cv2
import argparse
import numpy as np
from services.FaceDetectionPytorch import FaceDetectYOLOPytorch
from timeit import default_timer as timer
import matplotlib.pyplot as plt

face_detection = FaceDetectYOLOPytorch()

def change_point ( centerx,centery,pointx,pointy,angle):
    anglex=angle/57.2957795
    newpointx =int(centerx+ ((pointx-centerx)*math.cos(anglex)-(pointy-centery)*math.sin(anglex)))
    newpointy= int(centery+((pointx-centerx)*math.sin(anglex)+ (pointy-centery)*math.cos(anglex)))
    return (newpointx,newpointy)
def cut_yolo(path_test):
    count=0
    names = os.listdir(path_test)
    for name in names:
        path_image = os.path.join(path_test,name)
        img_raw = cv2.imread(path_image,cv2.IMREAD_COLOR)
        print(path_image)
        boxs, landms, class_names = face_detection.find_bbox([img_raw])
        if boxs!=None:
            for (box, landm, cls_name) in zip(boxs, landms, class_names):
                face = list(map(int, box))
                land = list(map(int, landm))       
                # rotated image
                height, width = img_raw.shape[:2]
                center = (width/2, height/2)
                angle=math.atan((land[3]-land[1])/(land[2]-land[0]))*57.2957795
                rotate_matrix = cv2.getRotationMatrix2D(center=center, angle=angle, scale=1)
                img_raw= cv2.warpAffine(src=img_raw, M=rotate_matrix, dsize=(width, height)) 
            
                
                # rotate_point

                (land[0], land[1])=change_point(center[0],center[1],land[0], land[1],-angle)
                (land[2], land[3])=change_point(center[0],center[1],land[2], land[3],-angle)
                (land[4], land[5])=change_point(center[0],center[1],land[4], land[5],-angle)
                (land[6], land[7])=change_point(center[0],center[1],land[6], land[7],-angle)
                (land[8], land[9])=change_point(center[0],center[1],land[8], land[9],-angle)
                (face[0], face[1])=change_point(center[0],center[1],face[0], face[1],-angle)
                (face[2], face[3])=change_point(center[0],center[1],face[2], face[3],-angle)

                cropped=img_raw[int(face[1]): int(face[3]),int (face[0]): int(face[2])]
                '''os.chdir(root_dir+outDirs[0])
                cv2.imwrite(name,cropped)
                cv2.waitKey(0)'''
                                                
                height_cropped,width_cropped= cropped.shape[:2]
                expandw=int(float((land[2]-land[0])/(face[2]-face[0]))*0.3*width_cropped)
                expandh=expandw


                eyes1=img_raw[land[1]-expandh:land[1]+expandh,land[0]-expandw:land[0]+expandw]
                plt.imshow(eyes1)
                plt.show()
                '''os.chdir(root_dir+outDirs[1])
                cv2.imwrite('eye_1_'+name,eyes1)
                cv2.waitKey(0)'''
                
                eyes2=img_raw[land[3]-expandh:land[3]+expandh,land[2]-expandw:land[2]+expandw]
                plt.imshow(eyes2)
                plt.show()
                '''os.chdir(root_dir+outDirs[1])
                cv2.imwrite('eye_2_'+name,eyes2)
                cv2.waitKey(0)'''

                '''peyes=img_raw[land[1]-expandh:land[3]+expandh,land[0]-expandw:land[2]+expandw]
                os.chdir(root_dir+outDirs[3])
                cv2.imwrite('2eyes '+name,peyes)
                cv2.waitKey(0)'''
                
    
if __name__ == "__main__":
    root_dir = os.path.dirname(os.path.realpath(__file__))
    outDirs = ['/view','/eye','/2eyes']
    path_test="D:/Open_close_eyes/testset"
    cut_yolo(path_test)
