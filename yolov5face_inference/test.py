import torch
import torch.nn as nn
import torch.utils.data
import torchvision
from torchvision import transforms
import os
from PIL import Image
import matplotlib.pyplot as plt
from ast import BitXor
import math
import cv2
import argparse
import numpy as np
from services.FaceDetectionPytorch import FaceDetectYOLOPytorch
from timeit import default_timer as timer

face_detection = FaceDetectYOLOPytorch()

img_test_transforms = transforms.Compose([
    transforms.Resize((224,224)),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225] )
    ])

def find_classes(dir):
    classes = os.listdir(dir)
    classes.sort()
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx

def change_point ( centerx,centery,pointx,pointy,angle):
    anglex=angle/57.2957795
    newpointx =int(centerx+ ((pointx-centerx)*math.cos(anglex)-(pointy-centery)*math.sin(anglex)))
    newpointy= int(centery+((pointx-centerx)*math.sin(anglex)+ (pointy-centery)*math.cos(anglex)))
    return (newpointx,newpointy)

if torch.cuda.is_available():
    device = torch.device("cuda") 
else:
    device = torch.device("cpu")
model = torchvision.models.resnet18(pretrained=True)
model.fc = nn.Sequential(nn.Linear(model.fc.in_features,512),nn.ReLU(), nn.Dropout(0.5), nn.Linear(512, 2))
model.load_state_dict(torch.load('D:/Open_close_eyes/weight/lr_005_bat_8_rgb.pth'))
model.eval()

def test_image(model, test_dir):
    
    labels, _ = find_classes('D:/Detect_eye_classification/test_set_classify_')
    
    names = os.listdir(test_dir)
    for name in names:
        path_image = os.path.join(test_dir,name)
        img_raw = cv2.imread(path_image, cv2.IMREAD_COLOR)
        cv2.imshow("Image", img_raw)
        cv2.waitKey(0)
        print(path_image)
        boxs, landms, class_names = face_detection.find_bbox([img_raw])
        if boxs!=None:
            for (box, landm, cls_name) in zip(boxs, landms, class_names):
                face = list(map(int, box))
                land = list(map(int, landm))       
                # rotated image
                height, width = img_raw.shape[:2]
                center = (width/2, height/2)
                angle=math.atan((land[3]-land[1])/(land[2]-land[0]))*57.2957795
                rotate_matrix = cv2.getRotationMatrix2D(center=center, angle=angle, scale=1)
                img_raw= cv2.warpAffine(src=img_raw, M=rotate_matrix, dsize=(width, height))                
                
                # rotate_point
                (land[0], land[1])=change_point(center[0],center[1],land[0], land[1],-angle)
                (land[2], land[3])=change_point(center[0],center[1],land[2], land[3],-angle)
                (land[4], land[5])=change_point(center[0],center[1],land[4], land[5],-angle)
                (land[6], land[7])=change_point(center[0],center[1],land[6], land[7],-angle)
                (land[8], land[9])=change_point(center[0],center[1],land[8], land[9],-angle)
                (face[0], face[1])=change_point(center[0],center[1],face[0], face[1],-angle)
                (face[2], face[3])=change_point(center[0],center[1],face[2], face[3],-angle)

                cropped=img_raw[int(face[1]): int(face[3]),int (face[0]): int(face[2])]
                '''os.chdir(root_dir+outDirs[0])
                cv2.imwrite(name,cropped)
                cv2.waitKey(0)'''
                                                
                height_cropped,width_cropped= cropped.shape[:2]
                expandw=int(float((land[2]-land[0])/(face[2]-face[0]))*0.3*width_cropped)
                expandh=expandw


                eyes1=img_raw[land[1]-expandh:land[1]+expandh,land[0]-expandw:land[0]+expandw]
                eyes1 = Image.fromarray(eyes1)
                eyes1 = img_test_transforms(eyes1)
                eyes1 = eyes1.unsqueeze(0)
                prediction1 = model(eyes1.to(device))
                prediction1 = prediction1.argmax()
                
                eyes2=img_raw[land[3]-expandh:land[3]+expandh,land[2]-expandw:land[2]+expandw]
                eyes2 = Image.fromarray(eyes2)
                eyes2 = img_test_transforms(eyes2)
                eyes2 = eyes2.unsqueeze(0)
                prediction2 = model(eyes2.to(device))
                prediction2 = prediction2.argmax()

                if (labels[prediction1] == "open" and labels[prediction2] == "open" ):
                    print("open")
                if (labels[prediction1] == "close" and labels[prediction2]== "close" ):
                    print("close")

                '''peyes=img_raw[land[1]-expandh:land[3]+expandh,land[0]-expandw:land[2]+expandw]
                os.chdir(root_dir+outDirs[3])
                cv2.imwrite('2eyes '+name,peyes)
                cv2.waitKey(0)'''
    
if __name__ == "__main__":
    test_image(model,"D:/Open_close_eyes/testset")