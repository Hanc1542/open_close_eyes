# Dataset utils and dataloaders

import glob
import math
import os
import re

import cv2, time
import numpy as np
import torch
import torchvision
from torchvision.ops import nms


def load_and_resize(img, dst_size):
    """load an image and resize to the dst_size

    Args:
        img (array): input image
        dst_size (tuple): (W, H)

    Returns:
        array: proccessed image
    """
    height, width = img.shape[:2]
    scale = min(dst_size[0]/width, dst_size[1]/height)
    new_size = (int(scale*height), int(scale*width))

    out_img = cv2.resize(img, (new_size[1], new_size[0]))
    delta_w = dst_size[0] - new_size[1]
    delta_h = dst_size[1] - new_size[0]
    top, bottom = delta_h//2, delta_h-(delta_h//2)
    left, right = delta_w//2, delta_w-(delta_w//2)
    out_img = cv2.copyMakeBorder(out_img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=[0, 0, 0])

    return out_img.astype("float32")

def make_divisible(x, divisor):
    # Returns x evenly divisible by divisor
    return math.ceil(x / divisor) * divisor

def check_img_size(img_size, s=32):
    new_size = make_divisible(img_size, int(s))  # ceil gs-multiple
    if new_size != img_size:
        print('WARNING: --img-size %g must be multiple of max stride %g, updating to %g' % (img_size, s, new_size))
    return new_size

def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = x.clone() if isinstance(x, torch.Tensor) else np.copy(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
    return y

def scale_boxes(boxes, landmarks, src_shape, dst_shape):
    scale = min(dst_shape[0]/src_shape[0], dst_shape[1]/src_shape[1])
    padding = (dst_shape[1]-src_shape[1]*scale) / 2, (dst_shape[0]-src_shape[0]*scale) / 2 
    boxes[:, [0, 2]] -= padding[0]
    boxes[:, [1 ,3]] -= padding[1]
    landmarks[:, ::2] -= padding[0]
    landmarks[:, 1::2] -= padding[1]
    boxes[:, :4] /= scale
    landmarks[:, :10] /= scale
    
    np.clip(boxes[:, [0, 2]], 0, src_shape[1])
    np.clip(boxes[:, [1, 3]], 0, src_shape[0])
    np.clip(landmarks[:, [0, 2]], 0, src_shape[1])
    np.clip(landmarks[:, [1, 3]], 0, src_shape[0])

    return boxes, landmarks

def check_iou(boxA, boxB):
    polygon = Polygon([(boxB[0], boxB[1]), (boxB[2], boxB[1]), (boxB[2], boxB[3]), (boxB[0], boxB[3])])
    boxA_x = (int(boxA[0]) + int(boxA[2]))/2
    boxA_y = (int(boxA[1]) + int(boxA[3]))/2
    point = Point(boxA_x, boxA_y)
    return polygon.contains(point)

def rm_smallboxes(boxes,landms,clsnames, cam_id, roi_nodetect, min_box ):
    bboxes =[]
    landmarks =[]
    labels =[]
    for (bbox, landm, cls_name ) in zip(boxes,landms, clsnames):
        
        if (cam_id != None) and len(roi_nodetect[cam_id]) != 0:
            if check_iou(bbox, roi_nodetect[cam_id])==True:
                continue
        if abs(bbox[2]-bbox[0]) >= min_box:
            bboxes.append(bbox)
            landmarks.append(landm)
            labels.append(cls_name)
    return bboxes, landmarks, labels


def non_max_suppression_face(prediction, conf_thres=0.25, iou_thres=0.45, classes=None, agnostic=False, labels=()):
    """Performs Non-Maximum Suppression (NMS) on inference results
    Returns:
         detections with shape: nx6 (x1, y1, x2, y2, conf, cls)
    """

    nc = prediction.shape[2] - 15  # number of classes
    xc = prediction[..., 4] > conf_thres  # candidates

    # Settings
    min_wh, max_wh = 2, 608 #4096  # (pixels) minimum and maximum box width and height

    redundant = True  # require redundant detections
    multi_label = nc > 1  # multiple labels per box (adds 0.5ms/img)
    merge = False  # use merge-NMS

    output = [torch.zeros((0, 16), device=prediction.device)] * prediction.shape[0]
    for xi, x in enumerate(prediction):  # image index, image inference
        x = x[xc[xi]]  # confidence

        if labels and len(labels[xi]):
            l = labels[xi]
            v = torch.zeros((len(l), nc + 15), device=x.device)
            v[:, :4] = l[:, 1:5]  # box
            v[:, 4] = 1.0  # conf
            v[range(len(l)), l[:, 0].long() + 15] = 1.0  # cls
            x = torch.cat((x, v), 0)

        if not x.shape[0]:
            continue

        x[:, 15:] *= x[:, 4:5] 
        box = xywh2xyxy(x[:, :4])
        if multi_label:
            i, j = (x[:, 15:] > conf_thres).nonzero(as_tuple=False).T
            x = torch.cat((box[i], x[i, j + 15, None], x[i, 5:15] ,j[:, None].float()), 1)
        else: 
            conf, j = x[:, 15:].max(1, keepdim=True)
            x = torch.cat((box, conf, x[:, 5:15], j.float()), 1)[conf.view(-1) > conf_thres]
        if classes is not None:
            x = x[(x[:, 5:6] == torch.tensor(classes, device=x.device)).any(1)]

        n = x.shape[0]
        if not n:
            continue

        # Batched NMS
        c = x[:, 15:16] * (0 if agnostic else max_wh)  # classes
        boxes, scores = x[:, :4] + c, x[:, 4]  # boxes (offset by class), scores
        i = torchvision.ops.nms(boxes, scores, iou_thres)  # NMS
        #if i.shape[0] > max_det:  # limit detections
        #    i = i[:max_det]
        if merge and (1 < n < 3E3):  # Merge NMS (boxes merged using weighted mean)
            iou = box_iou(boxes[i], boxes) > iou_thres  # iou matrix
            weights = iou * scores[None]  # box weights
            x[i, :4] = torch.mm(weights, x[:, :4]).float() / weights.sum(1, keepdim=True)  # merged boxes
            if redundant:
                i = i[iou.sum(1) > 1]  # require redundancy

        output[xi] = x[i]
        
    return output

