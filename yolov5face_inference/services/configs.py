from pathlib import Path

# Load yml file
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import os

config_path = Path('.') / 'development_config.yml'

class Config(object):
    def __init__(self):
        # Config Flask
        configs = load(open(config_path, 'r'), Loader=Loader)
        
        # Device
        self.USE_CUDA = configs['USE_CUDA']
        self.DEVICE_ID = configs['DEVICE_ID']

        # Pytorch Yolo5face
        self.YOLO5FACE_PYTORCH_PATH = configs['YOLO5FACE_PYTORCH_PATH']
        self.YOLO5FACE_MODEL_WH = configs['YOLO5FACE_MODEL_WH']
        self.YOLO5FACE_CONF_THRESHOLD = configs['YOLO5FACE_CONF_THRESHOLD']
        self.YOLO5FACE_IOU_THRESHOLD = configs['YOLO5FACE_IOU_THRESHOLD']
        
    
        

        


