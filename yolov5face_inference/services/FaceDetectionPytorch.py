import copy
import sys
import math
import json
import uuid
import cv2
import numpy as np
from numpy import random
import torch
import torch.backends.cudnn as cudnn
sys.path.append('services')
from models.experimental import attempt_load
from utils.utils import *
from configs import Config

class FaceDetectYOLOPytorch(object):
    def __init__(self):
        self.configs = Config()
        if self.configs.USE_CUDA:
            os.environ["CUDA_VISIBLE_DEVICES"] = self.configs.DEVICE_ID
        self.device = torch.device("cuda" if self.configs.USE_CUDA else "cpu")
        self.model = self._load_model()
        self.stride_model = self.model.stride.max()
        print('Finished loading model face detection!')
    
    def __call__(self, images):
        return self.find_bbox(images)
    
    def find_bbox(self, images):
        preprocess_batch = [self._preprocess(img)[0] for img in images]
        if self.configs.USE_CUDA:
            preds = self.model(torch.stack(preprocess_batch).cuda())
        else:
            preds = self.model(torch.stack(preprocess_batch))
        bboxs, landmarks, class_nums = self._postprocess( images, self.configs.YOLO5FACE_MODEL_WH[::-1], preds )
        return bboxs[0], landmarks[0], class_nums[0]
    
    def _load_model(self):
        model = attempt_load (self.configs.YOLO5FACE_PYTORCH_PATH, map_location=self.device)  # load FP32 model
        return model
    
    def _preprocess(self, img_raw):

        img = load_and_resize(img_raw, self.configs.YOLO5FACE_MODEL_WH)
        img = img[:, :, ::-1].transpose(2, 0, 1).copy()
        img = torch.from_numpy(img).to(self.device)
        img /= 255.0
        
        if img.ndimension() == 3:
            img = img.unsqueeze(0)
        
        return img
    
    def _postprocess(self, imgs_raw, img_shape, preds):
        dets, landms , classes = list(), list(),  list()
        
        for (pred, img_raw) in zip(preds[0], imgs_raw):
            pred= pred.reshape(1, pred.shape[0], pred.shape[1])
            pred = non_max_suppression_face(pred, self.configs.YOLO5FACE_CONF_THRESHOLD, self.configs.YOLO5FACE_IOU_THRESHOLD)
            
            bboxs =[]
            landmarks =[]
            class_nums =[]
            for i, det in enumerate(pred):
                if len(det):
                    xywh, lands = scale_boxes( det[:, :5].cpu().numpy(), det[:, 5:15].cpu().numpy(), img_raw.shape, img_shape)
                    bboxs = xywh.tolist()
                    landmarks = lands.tolist()
                    class_num = det[:, 15].cpu().numpy()
                    class_nums = (class_num.astype(bool)).tolist()
                    
            if len(bboxs) == 0:
                dets.append([])
                landms.append([])
                classes.append([])
                
            else:
                dets.append(bboxs)
                landms.append(landmarks)
                classes.append(class_nums)
                   
        return dets, landms, classes

