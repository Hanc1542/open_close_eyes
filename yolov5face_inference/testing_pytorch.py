
from ast import BitXor
import os
import cv2
import argparse
import numpy as np
from services.FaceDetectionPytorch import FaceDetectYOLOPytorch
from timeit import default_timer as timer


face_detection = FaceDetectYOLOPytorch()

def test_pytorch():

    img_raw = cv2.imread('D:/Open_close_eyes/testset/chup-anh-chan-dung-troi-nang-6.jpg', cv2.IMREAD_COLOR)
    # cv2.resize(img_raw,(100,200))
    boxs, landms, class_names = face_detection.find_bbox([img_raw])
    if boxs!=None:
        for (box, landm, cls_name) in zip(boxs, landms, class_names):
            face = list(map(int, box))
            land = list(map(int, landm))
            
            cv2.rectangle( img_raw, (face[0], face[1]), (face[2], face[3]), (0,0,255), 5)
            cv2.circle(img_raw, (land[0], land[1]), 1, (0, 255, 255), 4) 
            cv2.circle(img_raw, (land[2], land[3]), 1, (0, 255, 255), 4) 
            cv2.circle(img_raw, (land[4], land[5]), 1, (0, 0, 255), 4)
            cv2.circle(img_raw, (land[6], land[7]), 1, (0, 0, 255), 4) 
            cv2.circle(img_raw, (land[8], land[9]), 1, (0, 0, 255), 4)
        #cv2.imwrite('image.jpg',img_raw) 
        cv2.imshow('image.jpg',img_raw)
        cv2.waitKey(0)
if __name__ == "__main__":
    test_pytorch()
